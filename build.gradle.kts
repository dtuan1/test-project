import com.kageiit.jacobo.JacoboTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot").version("3.1.2")
    id("io.spring.dependency-management").version("1.1.2")
    id("com.apollographql.apollo").version("2.5.14")
    id("com.kageiit.jacobo") version "2.1.0"
    kotlin("jvm").version("1.9.0")
    kotlin("plugin.spring").version("1.9.0")
    jacoco
}

group = "group.phorus"
description = "Kotlin / Spring Boot based GraphQL service"
version = "1.0.0"

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
    withSourcesJar()
    withJavadocJar()
}

repositories {
    mavenCentral()
}

val validationApiVersion by extra("2.0.0.Final")
val graphqlValidationVersion by extra("19.1-hibernate-validator-6.2.0.Final")
val coroutinesCoreVersion by extra("1.7.2")
val mockitoKotlinVersion by extra("5.0.0")
val kotlinTestVersion by extra("1.9.0")
val junitPlatformVersion by extra("1.10.0")
val cucumberVersion by extra("7.13.0")

dependencies {
    // Spring specific
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-graphql")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("javax.validation:validation-api:$validationApiVersion")
    implementation("com.graphql-java:graphql-java-extended-validation:$graphqlValidationVersion")

    // JPA
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    runtimeOnly("com.h2database:h2")
    runtimeOnly("org.postgresql:postgresql")

    // Kotlin
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-jvm:$coroutinesCoreVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesCoreVersion")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.graphql:spring-graphql-test")
    testImplementation("org.mockito.kotlin:mockito-kotlin:$mockitoKotlinVersion")
    testImplementation("org.jetbrains.kotlin:kotlin-test:$kotlinTestVersion")
    testImplementation("org.junit.platform:junit-platform-suite:$junitPlatformVersion")
    testImplementation("io.cucumber:cucumber-java:$cucumberVersion")
    testImplementation("io.cucumber:cucumber-junit-platform-engine:$cucumberVersion")
    testImplementation("io.cucumber:cucumber-spring:$cucumberVersion")
}

springBoot {
    mainClass.set("group.phorus.testservice.TestServiceApplicationKt")
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = java.targetCompatibility.toString()
        }
    }

    // Jacoco config
    jacocoTestReport {
        executionData.setFrom(fileTree(buildDir).include("/jacoco/*.exec"))

        reports {
            xml.required.set(true)
            csv.required.set(true)
        }

        finalizedBy("jacobo")
    }

    register<JacoboTask>("jacobo") {
        description = "Transforms jacoco xml report to cobertura"
        group = "verification"

        jacocoReport = file("$buildDir/reports/jacoco/test/jacocoTestReport.xml")
        coberturaReport = file("$buildDir/reports/cobertura/cobertura.xml")
        includeFileNames = emptySet()

        val field = JacoboTask::class.java.getDeclaredField("srcDirs")
        field.isAccessible = true
        field.set(this, sourceSets["main"].allSource.srcDirs.map { it.path }.toTypedArray())

        dependsOn(jacocoTestReport)
    }

    // Merge jacoco reports and convert the xml report to cobertura.xml
    register<JacoboTask>("jacoboMerge") {
        description = "Transforms jacoco merged xml report to cobertura"
        group = "verification"

        jacocoReport = file("$buildDir/reports/jacoco/jacocoMergeReports/jacocoMergeReports.xml")
        coberturaReport = file("$buildDir/reports/cobertura/coberturaMerge.xml")
        includeFileNames = setOf()

        val field = JacoboTask::class.java.getDeclaredField("srcDirs")
        field.isAccessible = true
        field.set(this, sourceSets["main"].allSource.srcDirs.map { it.path }.toTypedArray())
    }

    register<JacocoReport>("jacocoMergeReports") {
        description = "Merge all jacoco reports"
        group = "verification"

        additionalSourceDirs.setFrom(files(sourceSets["main"].allSource.srcDirs).files.first())
        sourceDirectories.setFrom(files(sourceSets["main"].allSource.srcDirs).files.first())
        classDirectories.setFrom(files(sourceSets["main"].output))
        executionData.setFrom(fileTree(buildDir).include("/jacoco/*.exec"))

        reports {
            xml.required.set(true)
            csv.required.set(true)
        }

        finalizedBy("jacoboMerge")
    }
    ////

    withType<Test> {
        useJUnitPlatform()

        // If parallel tests start failing, instead of disabling this, take a look at @Execution(ExecutionMode.SAME_THREAD)
        systemProperty("junit.jupiter.execution.parallel.enabled", "true")
        systemProperty("junit.jupiter.execution.parallel.mode.default", "same_thread")
        systemProperty("junit.jupiter.execution.parallel.mode.classes.default", "concurrent")

        finalizedBy(jacocoTestReport)
    }

    register<Test>("unitTest") {
        description = "Run unit tests only"
        group = "verification"

        useJUnitPlatform {
            exclude("**/CucumberIntegrationTest.class")
        }

        // If parallel tests start failing, instead of disabling this, take a look at @Execution(ExecutionMode.SAME_THREAD)
        systemProperty("junit.jupiter.execution.parallel.enabled", "true")
        systemProperty("junit.jupiter.execution.parallel.mode.default", "same_thread")
        systemProperty("junit.jupiter.execution.parallel.mode.classes.default", "concurrent")

        finalizedBy(jacocoTestReport)
    }

    register<Test>("integrationTest") {
        description = "Run integration tests only"
        group = "verification"

        useJUnitPlatform {
            include("**/CucumberIntegrationTest.class")
        }

        // If parallel tests start failing, instead of disabling this, take a look at @Execution(ExecutionMode.SAME_THREAD)
        systemProperty("junit.jupiter.execution.parallel.enabled", "true")
        systemProperty("junit.jupiter.execution.parallel.mode.default", "same_thread")
        systemProperty("junit.jupiter.execution.parallel.mode.classes.default", "concurrent")

        finalizedBy(jacocoTestReport)
    }
}

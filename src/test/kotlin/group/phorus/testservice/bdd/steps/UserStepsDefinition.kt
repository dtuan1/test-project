package group.phorus.testservice.bdd.steps

import com.fasterxml.jackson.databind.ObjectMapper
import group.phorus.testservice.bdd.RequestScenarioScope
import group.phorus.testservice.bdd.ResponseScenarioScope
import group.phorus.testservice.model.dbentities.User
import group.phorus.testservice.repositories.UserRepository
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import org.springframework.beans.factory.annotation.Autowired
import java.util.*
import kotlin.test.assertTrue
import kotlin.test.fail

internal class UserStepsDefinition(
    @Autowired private val userRepository: UserRepository,
    @Autowired private val requestScenarioScope: RequestScenarioScope,
    @Autowired private val responseScenarioScope: ResponseScenarioScope,
    @Autowired private val objectMapper: ObjectMapper,
) {
    @Given("someone has a mutation to create a user")
    fun `someone has a mutation to create a user`() {
        requestScenarioScope.apply {
            query = """
                mutation {
                  createUser(user: {
                    email: "${UUID.randomUUID()}@email.com",
                    name: "testName",
                  })
                }
            """.trimIndent()
            queryType = "createUser"
        }
    }

    @Given("someone has a mutation to update a user{byTheId}")
    fun `someone has a mutation to update a user`(byTheId: String?) {
        requestScenarioScope.apply {
            query = """
                mutation {
                  updateUser(id: "${byTheId ?: "1"}", user: {
                    email: "${UUID.randomUUID()}@email.com",
                    name: "testName",
                  })
                }
            """.trimIndent()
            queryType = "updateUser"
        }
    }

    @Given("someone has a mutation to delete a user")
    fun `someone has a mutation to delete a user`() {
        val id = userRepository.save(
            User(
                email = "${UUID.randomUUID()}@email.com",
                name = "test",
            )
        ).id!!

        requestScenarioScope.apply {
            query = """
                mutation {
                  deleteUser(id: "1")
                }
            """.trimIndent()
            queryType = "deleteUser"
        }
    }


    @Then("the users returned have posts and comments")
    fun `the users returned have posts and comments`() {
        val response = with (requestScenarioScope.queryType!!) { when {
            endsWith("s") -> {
                val first = (responseScenarioScope.response as List<*>).first()
                objectMapper.convertValue(first, User::class.java)
            }
            endsWith("ById") -> objectMapper.convertValue(responseScenarioScope.response, User::class.java)
            else -> fail(("Unrecognized query type"))
        }}

        assertTrue(response.comments.isNotEmpty())
        assertTrue(response.posts.isNotEmpty())
    }
}
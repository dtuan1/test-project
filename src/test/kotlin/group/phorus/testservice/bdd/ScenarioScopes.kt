package group.phorus.testservice.bdd

import io.cucumber.spring.ScenarioScope
import org.springframework.stereotype.Component

@Component
@ScenarioScope
internal class RequestScenarioScope(
    var query: String? = null,
    var queryType: String? = null,
)

@Component
@ScenarioScope
internal class ResponseScenarioScope(
    var response: Any? = null,
)
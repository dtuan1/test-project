package group.phorus.testservice.services.impl

import group.phorus.testservice.model.dbentities.User
import group.phorus.testservice.model.dtos.UserDTO
import group.phorus.testservice.repositories.UserRepository
import group.phorus.testservice.services.UserService
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.kotlin.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.Pageable
import org.springframework.test.context.ActiveProfiles
import java.util.*
import kotlin.jvm.optionals.getOrNull
import kotlin.test.*


@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserServiceImplTest(
    @Autowired private val userRepository: UserRepository,
    @Autowired private val userService: UserService,
) {

    @Test
    fun `find all users`(): Unit = runBlocking {
        val response = userService.findAll(Pageable.unpaged())

        assertTrue(response.size > 0)
    }

    @Test
    fun `find user by id`(): Unit = runBlocking {
        val id = userRepository.findAll().first().id!!

        val response = userService.findById(id)

        assertEquals(id, response.id)
    }

    @Test
    fun `findById - user not found`(): Unit = runBlocking {
        val id = UUID.randomUUID().toString()
        try {
            userService.findById(id)
            fail("Exception not thrown")
        } catch (ex: Exception) {
            assertEquals("User not found with id: $id", ex.message)
        }
    }

    @Test
    fun `create a user`(): Unit = runBlocking {
        val response = userService.create(createUserDto())

        val saved = userRepository.findById(response).getOrNull()
        assertNotNull(saved)
    }

    @Test
    fun `update a user`(): Unit = runBlocking {
        val user = userRepository.findAll().first()

        userService.update(user.id!!, UserDTO(name = "test2", email = "testtest@email.com"))

        val updated = userRepository.findById(user.id!!).get()

        assertEquals("test2", updated.name)
        assertEquals("testtest@email.com", updated.email)
    }

    @Test
    fun `update - only name`(): Unit = runBlocking {
        val user = userRepository.findAll().first()

        userService.update(user.id!!, UserDTO(name = "test2"))

        val updated = userRepository.findById(user.id!!).get()

        assertEquals("test2", updated.name)
        assertNotNull(updated.email)
    }

    @Test
    fun `update - user not found`(): Unit = runBlocking {
        val id = UUID.randomUUID().toString()

        try {
            userService.update(id, createUserDto())
            fail("Exception not thrown")
        } catch (ex: Exception) {
            assertEquals("User not found with id: $id", ex.message)
        }
    }

    @Test
    fun `delete a user by id`(): Unit = runBlocking {
        val id = userRepository.save(
            User(
                email = "${UUID.randomUUID()}@email.com",
                name = "test",
            )
        ).id!!

        userService.deleteById(id)

        val deleted = userRepository.findById(id).getOrNull()

        assertNull(deleted)
    }

    private fun createUserDto(): UserDTO =
        UserDTO("test2@email.com", "testName2")
}
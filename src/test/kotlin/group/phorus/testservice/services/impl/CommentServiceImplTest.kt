package group.phorus.testservice.services.impl

import group.phorus.testservice.model.dbentities.Comment
import group.phorus.testservice.model.dtos.CommentDTO
import group.phorus.testservice.repositories.CommentRepository
import group.phorus.testservice.repositories.PostRepository
import group.phorus.testservice.repositories.UserRepository
import group.phorus.testservice.services.CommentService
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.kotlin.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.Pageable
import org.springframework.test.context.ActiveProfiles
import java.util.*
import kotlin.jvm.optionals.getOrNull
import kotlin.test.*


@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CommentServiceImplTest(
    @Autowired private val commentRepository: CommentRepository,
    @Autowired private val commentService: CommentService,
    @Autowired private val userRepository: UserRepository,
    @Autowired private val postRepository: PostRepository,
) {

    @Test
    fun `find all comments`(): Unit = runBlocking {
        val response = commentService.findAll(Pageable.unpaged())

        assertTrue(response.size > 0)
    }

    @Test
    fun `find comment by id`(): Unit = runBlocking {
        val id = commentRepository.findAll().first().id!!

        val response = commentService.findById(id)

        assertEquals(id, response.id)
    }

    @Test
    fun `findById - comment not found`(): Unit = runBlocking {
        val id = UUID.randomUUID().toString()
        try {
            commentService.findById(id)
            fail("Exception not thrown")
        } catch (ex: Exception) {
            assertEquals("Comment not found with id: $id", ex.message)
        }
    }

    @Test
    fun `find comment by author id`(): Unit = runBlocking {
        val id = userRepository.findAll().first().id!!

        val response = commentService.findAllByAuthorId(id, Pageable.unpaged())

        assertTrue(response.size > 0)
    }

    @Test
    fun `find comment by post id`(): Unit = runBlocking {
        val id = postRepository.findAll().first().id!!

        val response = commentService.findAllByPostId(id, Pageable.unpaged())

        assertTrue(response.size > 0)
    }

    @Test
    fun `create a comment`(): Unit = runBlocking {
        val authorId = userRepository.findAll().first().id!!
        val postId = postRepository.findAll().first().id!!
        val response = commentService.create(createCommentDto(), authorId, postId)

        val saved = commentRepository.findById(response).getOrNull()
        assertNotNull(saved)
    }

    @Test
    fun `update a comment`(): Unit = runBlocking {
        val comment = commentRepository.findAll().first()

        commentService.update(comment.id!!, CommentDTO(content = "test2"))

        val updated = commentRepository.findById(comment.id!!).get()

        assertEquals("test2", updated.content)
    }

    @Test
    fun `update - comment not found`(): Unit = runBlocking {
        val id = UUID.randomUUID().toString()

        try {
            commentService.update(id, createCommentDto())
            fail("Exception not thrown")
        } catch (ex: Exception) {
            assertEquals("Comment not found with id: $id", ex.message)
        }
    }

    @Test
    fun `delete a comment by id`(): Unit = runBlocking {
        val id = commentRepository.save(
            Comment(
                content = "test",
                author = userRepository.findAll().first(),
                post = postRepository.findAll().first(),
            )
        ).id!!

        commentService.deleteById(id)

        val deleted = commentRepository.findById(id).getOrNull()

        assertNull(deleted)
    }

    private fun createCommentDto(): CommentDTO =
        CommentDTO("testContent")
}
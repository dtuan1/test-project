Feature: Users can be created, updated, deleted, and fully queried
  You should be able to create, update, or delete any users, and
  all and any specific users should be able to be queried, including their posts and comments

  Scenario: Get all the users
    Given someone has a query to get all users
    When someone sends the query
    Then the service returns multiple users
    And the users returned have posts and comments

  Scenario Template: Get a user by id
    Given someone has a query to get a user by the id "<id>"
    When someone sends the query
    Then the service returns a single users
    And the users returned have posts and comments

    Examples:
      | id |
      |  1 |
      |  3 |
      |  5 |

  Scenario: Get a non-existent user by id
    Given someone has a query to get a user by the id "987654321"
    When someone sends the query
    Then the service returns an error with the message "User not found with id: 987654321"

  Scenario: Create a user
    Given someone has a mutation to create a user
    When someone sends the query
    Then the service returns the user id

  Scenario: Update a user by id
    Given someone has a mutation to update a user
    When someone sends the query
    Then the service returns the user id

  Scenario: Update a non-existent user by id
    Given someone has a mutation to update a user by the id "987654321"
    When someone sends the query
    Then the service returns an error with the message "User not found with id: 987654321"

  Scenario: Delete a user by id
    Given someone has a mutation to delete a user
    When someone sends the query
    Then the service returns the user id
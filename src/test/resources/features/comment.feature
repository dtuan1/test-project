Feature: Comments can be created, updated, deleted, and fully queried
  You should be able to create, update, or delete any comments, and
  all and any specific comments should be able to be queried, including its comments and author

  Scenario Template: Get a comment by id
    Given someone has a query to get a comment by the id "<id>"
    When someone sends the query
    Then the service returns a single comments
    And the comments returned have an author and a post

    Examples:
      | id |
      |  1 |
      |  3 |
      |  5 |

  Scenario: Get a non-existent comment by id
    Given someone has a query to get a comment by the id "987654321"
    When someone sends the query
    Then the service returns an error with the message "Comment not found with id: 987654321"

  Scenario: Create a comment
    Given someone has a mutation to create a comment
    When someone sends the query
    Then the service returns the comment id

  Scenario: Update a comment by id
    Given someone has a mutation to update a comment
    When someone sends the query
    Then the service returns the comment id

  Scenario: Update a non-existent comment by id
    Given someone has a mutation to update a comment by the id "987654321"
    When someone sends the query
    Then the service returns an error with the message "Comment not found with id: 987654321"

  Scenario: Delete a comment by id
    Given someone has a mutation to delete a comment
    When someone sends the query
    Then the service returns the comment id

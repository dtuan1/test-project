package group.phorus.testservice.repositories

import group.phorus.testservice.model.dbentities.Post
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.Optional


@Repository
interface PostRepository: JpaRepository<Post, String> {
    fun findAllByAuthorId(authorId: String, pageable: Pageable): Page<Post>
}

package group.phorus.testservice.services

import group.phorus.testservice.model.dbentities.Comment
import group.phorus.testservice.model.dtos.CommentDTO
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface CommentService : CrudService<Comment, CommentDTO, String> {
    suspend fun create(dto: CommentDTO, authorId: String, postId: String): String
    suspend fun findAllByAuthorId(userId: String, pageable: Pageable): Page<Comment>
    suspend fun findAllByPostId(postId: String, pageable: Pageable): Page<Comment>
}
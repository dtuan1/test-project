package group.phorus.testservice.services

import group.phorus.testservice.model.dbentities.Post
import group.phorus.testservice.model.dtos.PostDTO
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface PostService : CrudService<Post, PostDTO, String> {
    suspend fun create(dto: PostDTO, authorId: String): String
    suspend fun findAllByAuthorId(userId: String, pageable: Pageable): Page<Post>
}
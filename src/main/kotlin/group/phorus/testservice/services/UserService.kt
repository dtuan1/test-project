package group.phorus.testservice.services

import group.phorus.testservice.model.dbentities.User
import group.phorus.testservice.model.dtos.UserDTO

interface UserService : CrudService<User, UserDTO, String>
package group.phorus.testservice.controllers;

import group.phorus.testservice.model.dtos.CommentDTO
import group.phorus.testservice.services.CommentService
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.stereotype.Controller


@Controller
class CommentController(
    private val commentService: CommentService,
) {

    @QueryMapping
    suspend fun commentById(@Argument id: String) = commentService.findById(id)

    @MutationMapping
    suspend fun createComment(@Argument comment: CommentDTO, @Argument authorId: String, @Argument postId: String) =
        commentService.create(comment, authorId, postId)

    @MutationMapping
    suspend fun updateComment(@Argument id: String, @Argument comment: CommentDTO) =
        commentService.update(id, comment).let { id }

    @MutationMapping
    suspend fun deleteComment(@Argument id: String) =
        commentService.deleteById(id).let { id }
}

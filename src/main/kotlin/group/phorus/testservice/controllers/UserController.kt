package group.phorus.testservice.controllers;

import group.phorus.testservice.model.dbentities.User
import group.phorus.testservice.model.dtos.UserDTO
import group.phorus.testservice.services.CommentService
import group.phorus.testservice.services.PostService
import group.phorus.testservice.services.UserService
import org.springframework.data.domain.Pageable
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.graphql.data.method.annotation.SchemaMapping
import org.springframework.stereotype.Controller


@Controller
class UserController(
    private val userService: UserService,
    private val postService: PostService,
    private val commentService: CommentService,
) {

    @QueryMapping
    suspend fun users(@Argument page: Int, @Argument size: Int) =
        userService.findAll(Pageable.ofSize(size).withPage(page))

    @QueryMapping
    suspend fun userById(@Argument id: String) =
        userService.findById(id)

    @SchemaMapping
    suspend fun posts(user: User, @Argument page: Int, @Argument size: Int) =
        postService.findAllByAuthorId(user.id!!, Pageable.ofSize(size).withPage(page))

    @SchemaMapping
    suspend fun comments(user: User, @Argument page: Int, @Argument size: Int) =
        commentService.findAllByAuthorId(user.id!!, Pageable.ofSize(size).withPage(page))

    @MutationMapping
    suspend fun createUser(@Argument user: UserDTO) =
        userService.create(user)

    @MutationMapping
    suspend fun updateUser(@Argument id: String, @Argument user: UserDTO) =
        userService.update(id, user).let { id }

    @MutationMapping
    suspend fun deleteUser(@Argument id: String) =
        userService.deleteById(id).let { id }
}

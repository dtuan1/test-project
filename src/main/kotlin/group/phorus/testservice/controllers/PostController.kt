package group.phorus.testservice.controllers

import group.phorus.testservice.model.dbentities.Post
import group.phorus.testservice.model.dtos.PostDTO
import group.phorus.testservice.services.CommentService
import group.phorus.testservice.services.PostService
import org.springframework.data.domain.Pageable
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.graphql.data.method.annotation.SchemaMapping
import org.springframework.stereotype.Controller


@Controller
class PostController(
    private val postService: PostService,
    private val commentService: CommentService,
) {
    @QueryMapping
    suspend fun posts(@Argument page: Int, @Argument size: Int) =
        postService.findAll(Pageable.ofSize(size).withPage(page))

    @QueryMapping
    suspend fun postById(@Argument id: String) =
        postService.findById(id)

    @SchemaMapping
    suspend fun comments(post: Post, @Argument page: Int, @Argument size: Int) =
        commentService.findAllByPostId(post.id!!, Pageable.ofSize(size).withPage(page))

    @MutationMapping
    suspend fun createPost(@Argument post: PostDTO, @Argument authorId: String) =
        postService.create(post, authorId)

    @MutationMapping
    suspend fun updatePost(@Argument id: String, @Argument post: PostDTO) =
        postService.update(id, post).let { id }

    @MutationMapping
    suspend fun deletePost(@Argument id: String) =
        postService.deleteById(id).let { id }
}

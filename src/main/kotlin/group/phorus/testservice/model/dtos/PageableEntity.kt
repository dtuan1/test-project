package group.phorus.testservice.model.dtos

data class PageableEntity<T> (
    val id: T? = null,
    val page: Int? = null,
    val size: Int? = null,
)
package group.phorus.testservice.model.dtos

class UserDTO(
    var email: String? = null,
    var name: String? = null,
)
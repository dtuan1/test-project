package group.phorus.testservice.model.dtos

class CommentDTO(
    var content: String? = null,
)
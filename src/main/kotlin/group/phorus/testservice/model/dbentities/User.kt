package group.phorus.testservice.model.dbentities

import jakarta.persistence.*


@Entity
@Table(name = "users",
    indexes = [
        Index(name = "user_email_index", columnList = "email"),
    ]
)
class User(
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    var id: String? = null,

    @Basic(fetch = FetchType.LAZY)
    @Column(unique = true, nullable = false)
    var email: String? = null,

    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = false)
    var name: String? = null,

    @OneToMany(cascade = [CascadeType.REMOVE], mappedBy = "author")
    var posts: List<Post> = emptyList(),

    @OneToMany(cascade = [CascadeType.REMOVE], mappedBy = "author")
    var comments: List<Comment> = emptyList(),
)

package group.phorus.testservice.model.dbentities

import jakarta.persistence.*

@Entity
@Table(name = "posts")
class Post(
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    var id: String? = null,

    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = false)
    var title: String? = null,

    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = false)
    var content: String? = null,

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    var author: User? = null,

    @OneToMany(cascade = [CascadeType.REMOVE], mappedBy = "post")
    var comments: List<Comment> = emptyList(),
)
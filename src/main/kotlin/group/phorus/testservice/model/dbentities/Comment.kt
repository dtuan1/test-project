package group.phorus.testservice.model.dbentities

import jakarta.persistence.*

@Entity
@Table(name = "comments")
class Comment(
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    var id: String? = null,

    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = false)
    var content: String? = null,

    @ManyToOne(optional = false)
    @JoinColumn(name = "post_id", nullable = false)
    var post: Post? = null,

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    var author: User? = null,
)
package group.phorus.testservice.config

import graphql.GraphQLError
import graphql.validation.interpolation.MessageInterpolator
import graphql.validation.rules.ValidationEnvironment
import group.phorus.testservice.exceptions.BadRequestException
import org.springframework.stereotype.Component

@Component
class CustomMessageInterpolator : MessageInterpolator {
    override fun interpolate(
        message: String, map: Map<String, Any>,
        validationEnvironment: ValidationEnvironment,
    ): GraphQLError = BadRequestException(
        String.format(message, validationEnvironment.argument.name),
        listOf(validationEnvironment.location),
    )
}

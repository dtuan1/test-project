package group.phorus.testservice.config

import graphql.schema.idl.RuntimeWiring
import graphql.validation.rules.OnValidationErrorStrategy
import graphql.validation.rules.ValidationRules
import graphql.validation.schemawiring.ValidationSchemaWiring
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.graphql.execution.RuntimeWiringConfigurer


@Configuration
class GraphQLConfig {
    @Bean
    fun runtimeWiringConfigurer(messageInterpolator: group.phorus.testservice.config.CustomMessageInterpolator): RuntimeWiringConfigurer {
        val validationRules = ValidationRules.newValidationRules()
            .onValidationErrorStrategy(OnValidationErrorStrategy.RETURN_NULL)
            .messageInterpolator(messageInterpolator)
            .build()
        val schemaWiring = ValidationSchemaWiring(validationRules)
        return RuntimeWiringConfigurer { builder: RuntimeWiring.Builder ->
            builder.directiveWiring(
                schemaWiring
            ).build()
        }
    }
}


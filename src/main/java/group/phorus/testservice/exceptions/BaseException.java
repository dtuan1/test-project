package group.phorus.testservice.exceptions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import graphql.GraphQLError;

public abstract class BaseException extends RuntimeException implements GraphQLError {
    abstract String errorMessage();

    @Override
    public String getMessage() {
        return errorMessage();
    }

    @Override
    @JsonIgnore
    public StackTraceElement[] getStackTrace() {
        return super.getStackTrace();
    }
}
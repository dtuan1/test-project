FROM openjdk:18.0.2 as build
MAINTAINER Martin Rios

WORKDIR /app

RUN microdnf install findutils
ADD ./gradlew ./gradlew
ADD ./gradle ./gradle
RUN ./gradlew --version

ADD ./ ./
RUN ./gradlew build -x test

FROM openjdk:18.0.2 as startup
MAINTAINER  Martin Rios

WORKDIR /app

# Entrypoint
COPY --from=0 /app/build/libs/test-service-1.0.0.jar /app/app.jar

ENTRYPOINT ["java", "-jar", "/app/app.jar"]

HEALTHCHECK --interval=15s --start-period=45s CMD curl --head --silent --fail http://127.0.0.1:8080/actuator/health || exit 1

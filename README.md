# Test Project

<a href='https://gitlab.com/rios.martinivan/test-project/-/pipelines?ref=main'><img src='https://gitlab.com/rios.martinivan/test-project/badges/main/pipeline.svg'></a>
<a href='https://gitlab.com/rios.martinivan/test-project/-/pipelines?ref=main'><img src='https://gitlab.com/rios.martinivan/test-project/badges/main/coverage.svg'></a>

This is a GraphQL service test project based on Spring Boot and Kotlin.
It was prepared as an example of what's possible to do with these technologies, so it has
features such as Exception Handling, Input Validation, Unit tests, Integration tests, and
non-blocking capabilities, thanks to non-blocking capabilities thanks to Spring Webflux and Kotlin.

As a bonus, the project has been configured with a much-needed GitLab CI/CD pipeline to
maintain the main branch free of bugs and errors.

## Table of contents
- [How to run the project](#how-to-run-the-project)
  - [Docker](#docker)
  - [Gradle](#gradle)
- [Project Structure](#project-structure)
- [Exception Handling](#exception-handling)
  - [Input Validation](#input-validation)
- [Testing](#testing)
  - [Unit Tests](#unit-tests)
  - [Integration Tests](#integration-tests)
  - [Testing with a Client project](#testing-with-a-client-project)
- [Pipeline](#pipeline)
- [Other notes](#other-notes)
  - [Non-blocking capabilities and why it's important](#non-blocking-capabilities-and-why-its-important)
  - [Kotlin coroutines + Webflux](#kotlin-coroutines-webflux)
  - [Hibernate - Batch loading](#hibernate-batch-loading)
- [Authors and acknowledgment](#authors-and-acknowledgment)

***

## How to run the project

The project can be run in two ways: running it through Docker or running it locally with Gradle.

### Docker

If you have Docker installed, to run the project with docker you only need to use the docker-compose.yml file found in the project.

To do so, you can use the following command while standing inside the project directory:
```bash
docker-compose up
```

This will start a PostgreSQL database and the service itself.

### Gradle

To start the service using Gradle, you need to have Java JDK 17 or higher on your system.

Then, you can start the service using the command:
```bash
./gradlew bootRun --args='--spring.profiles.active=test'
```

The `test` profile of the service uses the H2 in-memory database to be able to run
without the need to have a database running locally.

Also, with this profile, you will be able to access GraphiQL through the URL: http://localhost:9090/graphiql

## Project Structure

- [main folder](src/main): Folder containing the main code of the project.
- [test folder](src/test): Folder containing the the project tests.
- [resources folder](src/main/resources): Folder containing the project resources, this includes the default and `test` profile properties of the project, the initial data to post in the database, and the GraphQL definitions.
- [ci-cd](ci-cd): Folder containing YMLs used by the pipeline in [.gitlab-ci.yml](.gitlab-ci.yml).
- [.gitlab-ci.yml](.gitlab-ci.yml): GitLab CI/CD pipeline jobs to build the project, and then run the unit and integration tests.
- [Dockerfile](Dockerfile): Dockerfile used to create an image ready to run the project.
- [docker-compose.yml](docker-compose.yml): Docker Compose definition to run the project locally.

## Exception Handling

The service has exception handling configured to capture some exceptions that may arise.  
Currently, three exceptions are captured: ConstraintViolationException, 
[BadRequestException](src/main/kotlin/group/phorus/testservice/exceptions/BadRequestException.kt) y
[ResourceNotFoundException](src/main/kotlin/group/phorus/testservice/exceptions/ResourceNotFoundException.kt).

### Input Validation

The ConstraintViolationException arises from errors caused by not respecting the directives
in [schema.graphqls](src/main/resources/graphql/schema.graphqls).  
These directives are configured automatically thanks to the dependency `com.graphql-java:graphql-java-extended-validation`.
This dependency configures directives for all Bean Validation Constraints, including
`@NotBlank`, `@NotNull`, `@NotEmpty`, `@Size`, and more.
The project currently only uses the `@NotBlank` constraint, but it's possible to use any other
constraint without problems.

## Testing

The project was created with TDD methodology in mind, so it has unit tests for each service class and integration
tests to validate the correct functioning of the complete service.

### Unit Tests

The [unit tests](src/test/kotlin/group/phorus/testservice/services) validate each service class
on a unitary and individual basis.

To run them locally, you need to have Java JDK 17 or higher on your system, and run the following command:
```bash
./gradlew unitTest
```

### Integration Tests

The [integration tests](src/test/kotlin/group/phorus/testservice/bdd) validate the service as a whole.
They run the service in the background and execute GraphQL queries and mutations to validate it works as expected.

The integration tests were created using Cucumber and use three feature files:
[user.feature](src/test/resources/features/user.feature), [post.feature](src/test/resources/features/post.feature) 
and [comment.feature](src/test/resources/features/comment.feature).

To run them locally, you need to have Java JDK 17 or higher on your system, and run the following command:
```bash
./gradlew integrationTest
```

### Testing with a Client Project

You can test the service by using the sample project [Test Project - GraphQL client](https://gitlab.com/rios.martinivan/test-project-graphql-client).
To do so, you need to run the project locally (See [How to run the project](#how-to-run-the-project)) and follow
the steps mentioned in [README.md](https://gitlab.com/rios.martinivan/test-project/-/blob/main/README.md).

## Pipeline

The project has a GitLab CI/CD pipeline configured with four jobs:
- `build`: This job builds the project and caches the built resources
  and binaries needed to deploy it later.
- `unit-test`: This job runs all unit tests.
- `integration-test`: This job runs all integration tests.
- `report-coverage`: This job takes the outputs from `unit-test` and `integration-test`, and reports the total
  test coverage to GitLab.

Keep in mind that the jobs run based on a directed acyclic graph defined according to the dependencies between them.

The `unit-test` and `integration-test` jobs depend on the `build` job, and the `report-coverage` job depends on
the first two. Therefore, the `build` job will run first, then the test jobs will run in parallel, and the
`report-coverage` job will run last.

## Other notes

Here we will mention some more technical data about the service, which is important to take into account.

<details open>
<summary>Non-blocking capabilities and why it's important</summary>

### Non-blocking capabilities and why it's important

In a conventional MVC application, whenever a request reaches the server, a servlet thread is being created
and delegated to worker threads to perform various operations like I/O, database processing, etc. While the worker
threads are busy completing their processes, the servlet threads enter a waiting state due to which the calls
remain blocked. This is blocking or synchronous request processing.

In a non-blocking system, all the incoming requests are accompanied by an event handler and a callback. The request
thread delegates the incoming request to a thread pool that manages a pretty small number of threads.
Then the thread pool delegates the request to its handler function and gets available to process the
next incoming requests from the request thread.

When the handler function completes its process, one of the threads from the pool fetches the response and passes it
to the callback function. However, the threads in a non-blocking system never go into the waiting state. This
increases the productivity and the performance of the application.

With this approach, a single request can be processed by multiple threads.

</details>

<details open>
<summary>Kotlin coroutines + Webflux</summary>

#### Kotlin coroutines + Webflux

Spring introduced a Multi-Event Loop model to enable a reactive stack known as WebFlux. It's a fully non-blocking
and annotation-based web framework built on Project Reactor which allows building reactive web applications on the
HTTP layer. It provides support for popular inbuilt severs like Netty (what's being used in this test project),
Undertow, and Servlet 3.1 containers.

Spring Webflux is usually complex to use since it makes you work with wrappers such as Mono and Flux, but when
programming with Kotlin instead of Java, it's possible to use Kotlin Coroutines instead of wrappers thanks to the
compatibility between Kotlin and Spring, which greatly simplifies the creation of non-blocking Spring based services.

</details>

<details open>
<summary>Hibernate - Batch loading</summary>

### Hibernate - Batch loading

Batch data loading is really important with GraphQL since the last thing you want is for a request to a service
to execute multiple SQL operations unnecessarily. It's therefore important to know how this works from the
point of view of the ORM you use, in our case, Hibernate.

Hibernate allows you to load data for associations using one of four fetching strategies: join, select,
subselect and batch. Out of these four strategies, batch loading allows for the biggest performance gains as it
is an optimization strategy for select fetching. In this strategy, Hibernate retrieves a batch of entity
instances or collections in a single SELECT statement by specifying a list of primary or foreign keys.

When Hibernate loads data on a per-class level, it requires the batch size of the association to
pre-load when queried. For example, consider that at runtime you have 30 instances of a post object loaded in session.
Each post object belongs to an user object. If you were to iterate through all the post objects and request
their users, with lazy loading, Hibernate will issue 30 select statements - one for each user.
This is a performance bottleneck.

You can instead, tell Hiberante to pre-load the data for the next batch of users before they
have been sought via a query. When an user object has been queried, Hibernate will query many
more of these objects in the same SELECT statement.

The number of user objects to query in advance depends upon the batch-size parameter specified at
configuration time. That's why we specified a `default_batch_fetch_size: 30`.

This tells Hibernate to query at least 30 more user objects in expectation of them being needed in
the near future. When a client queries the user of post A, the user of post B may already have been loaded
as part of batch loading. When the client actually needs the user of post B, instead of going to the
database (and issuing a SELECT statement), the value can be retrieved from the current session.

In addition to the batch-size parameter, Hibernate has introduced a new configuration item to improve in
batch loading performance. The configuration item is called Batch Fetch Style configuration and specified
by the `batch_fetch_style` parameter.

In the legacy style of loading, a set of pre-built batch sizes based on `ArrayHelper.getBatchSizes(int)` are utilized.
Batches are loaded using the next-smaller pre-built batch size from the number of existing batchable identifiers.

So, with a batch-size setting of 30, the pre-built batch sizes would be [30, 15, 10, 9, 8, 7, .., 1]. An attempt to
batch load 29 identifiers would result in batches of 15, 10, and 4. There will be 3 corresponding SQL queries,
each loading 15, 10 and 4 owners from the database.

With the new `dynamic` batch fetch style, while still conforming to batch-size restrictions, Hibernate dynamically
builds its SQL SELECT statement using the actual number of objects to be loaded.
For example, for 30 owner objects, and a maximum batch size of 30, a call to retrieve 30 owner objects
will result in one SQL SELECT statement. A call to retrieve 35 will result in two SQL statements, of batch sizes 30
and 5 respectively.

This is the reason why we specified `batch_fetch_style: dynamic`.

</details>

## Authors and acknowledgment

- [Martin Ivan Rios](https://linkedin.com/in/ivr2132)
